/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tap.practicas.ej20;

import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JFrame;
import javax.swing.*;

public class Practica1 extends JFrame implements ActionListener{

    JLabel etiq1;
    JTextField nombre ;
    JButton btn ;
    
    Practica1()
    {
        this.setTitle("Practica1");
        this.setSize(300,200);
        this.setLayout(new FlowLayout());
        
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        etiq1 = new JLabel("Escriba nombre para saludar");
        nombre = new JTextField(20);
        btn = new JButton("¡Salir!");
        
        btn.addActionListener(this);
        
        this.add(etiq1);
        this.add(nombre);
        this.add(btn);
    }
    
    
    public static void main(String[] args) 
    {
        /*Es un main del main , Crea y poner un formulario */
        java.awt.EventQueue.invokeLater(new Runnable(){
            public void run()
                {
                    new Practica1().setVisible(true);
                }
        });         
    }
       
    @Override
    public void actionPerformed(ActionEvent e) {
       JOptionPane.showMessageDialog(this,"Hola "+nombre.getText());
    }
}
