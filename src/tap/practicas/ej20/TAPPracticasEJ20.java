/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tap.practicas.ej20;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;

/**
 *
 * @author aldair
 */
public class TAPPracticasEJ20 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        JFrame frm = new JFrame("Ventana");
        frm.setSize(300,200);
        frm.setLayout(new FlowLayout());
        
        JButton btn = new JButton("Cerrar");
        
        btn.addActionListener((ActionEvent e) -> {
            System.exit(0);
        });
        
        frm.add(btn);
        
        frm.setVisible(true);
    }
}
